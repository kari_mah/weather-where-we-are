class CityData
  def initialize(city)
    @city = city
    @geocode = Geokit::Geocoders::GoogleGeocoder.geocode @city
    coordinates = @geocode.ll.split(',').map(&:to_i)
    @forecast = ForecastIO.forecast(coordinates[0], coordinates[1])
  end

  def geocode
    @geocode
  end

  def forecast
    @forecast
  end

  def weather
    forecast.currently.summary
  end

  def temperature
    forecast.currently.temperature
  end

  def full_address
    geocode.full_address
  end
end