require 'rubygems'
require 'sinatra'
require 'geokit'
require 'forecast_io'
require 'open-uri'
require 'rexml/document'
require 'cgi'
require './city_data'
require './photo.rb'

ForecastIO.configure do |configuration|
  configuration.api_key = 'a6f412f4fd589f6d6362ed20dacd86f7'
end

FLICKR_API_KEY = 'd3b0fc2f0ca8617c8b4b78d31dc1afac'

get '/' do
  erb :index
end

post '/' do
  @city_a = params[:city].to_s
  @city_b = params[:second_city].to_s

  if @city_a != '' && @city_b != ''
    a = CityData.new(@city_a)
    @a_summary = a.weather
    @a_temp = a.temperature.to_i
    @a_full_address = a.full_address

    b = CityData.new(@city_b)
    @b_summary = b.weather
    @b_temp = b.temperature.to_i
    @b_full_address = b.full_address

    @distance = a.geocode.distance_to(b.geocode).to_i
    @img_a = Photo.new.pick_a_photo(@city_a)
    @img_b = Photo.new.pick_a_photo(@city_b)
  end

  erb :index
end
