### Weather Where We Are

This application allows you to look up the current weather
in two different cities. You'll also find out how far apart the cities are.

Weather Where We Are relies on the [Geokit gem](https://github.com/geokit/geokit)
for geocoding and the [Forecast.io API](https://developer.forecast.io/) for
weather information and temperature. Photos are provided by [Flickr's API](https://www.flickr.com/services/api/).

This application came about when my partner went to Uzbekistan for a friend's wedding.
Few places on Earth seem more distant to me than Central Asia. Knowing what the weather
was like where he was brought me comfort.

## Installation
Run <code>bundle install</code> to install necessary gems.

## Execute 
Run <code>ruby app.rb</code> to run the app at [http://localhost:4567/]http://localhost:4567/

TODO:
- Add Javascript/HTML5 geolocation to grab geocode for user

